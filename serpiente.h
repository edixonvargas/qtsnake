#ifndef SERPIENTE_H
#define SERPIENTE_H

#include <QPoint>
#include "globales.h"
#include "mapa.h"

class Mapa;
class Serpiente : public QObject {

    Q_OBJECT

    private:
        //Otros miembros

        //Atributos de clase privados
        int maxLongitud;
        int minLongitud;
        int longitud;
        Direccion sentido;
        Mapa *mapAssoc;
        QTimer *tVelocidad;
        QPoint posActual;
        QList<QPoint> estructuraSerpiente;
        int minIntervalo;
        int maxIntervalo;

        //Metodos de clase privados
        bool hayAlimento(int x, int y);
        void comerAlimento(Alimento alim);
        int distDosPuntos(QPoint p1, QPoint p2);
        void iniciarSerpiente(Mapa *map);
        void iniciarSerpiente(Mapa *map, int largo, QPoint posicion);
        void verificarAlimento(Alimento alimento, QList<Alimento> *alimentos, QPoint proxPunto, int lSerpiente);

    public:
        explicit Serpiente(Mapa *map);
        explicit Serpiente(int largo, Mapa *map);

        void setLongitud(int longit);
        void setMaxLongitud(int maxlen);
        void setSentido(Direccion sent);
        void iniciarMovimiento();
        void detenerMovimiento();
        void asociarMapa(Mapa *maps);
        void alargaSerpiente(int cant);
        int longActual();
        QList<QPoint> getEstructura();
        void mover(Direccion dir);
        Direccion getSentido();
        int getLongitud();
        void setVelocidad(double vel);
        double getVelocidad();
        int getMinLongitud();
        void setMinLongitud(int min);
        int getMaxLongitud();
        void setMaxVelocidad(double maxvel);
        void setMinVelocidad(double minvel);
        void restaurarSerpiente();
        bool isMoving();


    public slots:
        void timeout();

    signals:
        void chocoObstaculo(int x, int y);
        void chocoObstaculo(QPoint qp);
        void comioAlimento(Alimento tipo);
};

#endif

