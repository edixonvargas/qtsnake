#include "jugador.h"

#define sepHorizontal 10

Jugador::Jugador(Juego *juego, QWidget *parent) : QWidget(parent), TURNOS_INICIALES(3){

    //Establece las condiciones iniciales de un jugador
    puntos = 0;
    nroTurnos = TURNOS_INICIALES - 1;
    juegoAssoc = juego;

    /**
Imprime la informacion inicial del jugador en el panel mismo
las instrucciones siguientes conllevan a mostrar un panel con los puntos
del jugador y los turnos restantes, todos ellos impresos al lado derecho del mapa
     */
    QFont fuente("Comic Sans MS", 30);
    fuente.setUnderline(true);

    QPalette pal(this->palette());
    pal.setColor(QPalette::WindowText, Qt::white);

    lblPuntos = new QLabel("Puntos", this);
    lblPuntos->setFont(fuente);
    lblPuntos->setPalette(pal);
    lblPuntos->setGeometry(sepHorizontal, 50, this->width() - sepHorizontal, 40);

    lblValPuntos = new QLabel(QString::number(puntos), this);
    fuente.setUnderline(false);
    lblValPuntos->setFont(fuente);
    lblValPuntos->setPalette(pal);
    lblValPuntos->setGeometry(lblPuntos->x(), lblPuntos->y() + lblPuntos->height() + 10, this->width() - sepHorizontal, 40);

    lblTurnos = new QLabel("Turnos", this);
    fuente.setUnderline(true);
    lblTurnos->setFont(fuente);
    lblTurnos->setPalette(pal);
    lblTurnos->setGeometry(sepHorizontal, lblPuntos->y() + lblPuntos->height() + 100, this->width() - sepHorizontal, 40);

    lblValTurnos = new QLabel(QString::number(nroTurnos), this);
    fuente.setUnderline(false);
    lblValTurnos->setFont(fuente);
    lblValTurnos->setPalette(pal);
    lblValTurnos->setGeometry(lblTurnos->x(), lblTurnos->y() + lblTurnos->height() + 50, this->width() - sepHorizontal, 40);

    serpControlada = juegoAssoc->getMapa()->getSerpiente();
    connect(serpControlada, SIGNAL(chocoObstaculo(QPoint)), this, SLOT(chocoObstaculoSerp(QPoint)));
    connect(serpControlada, SIGNAL(comioAlimento(Alimento)), this, SLOT(comioAlimento(Alimento)));
}

void Jugador::setGeometry(int x, int y, int w, int h){
     QWidget::setGeometry(x, y, w, h);
     lblPuntos->setGeometry(sepHorizontal, 50, w - sepHorizontal, 40);
     lblTurnos->setGeometry(sepHorizontal, 50, w - sepHorizontal, 400);
}

Jugador::~Jugador(){
}

void Jugador::setPuntos(int pts){
    puntos = pts;
}

int Jugador::getPuntos(){
    return puntos;
}

void Jugador::assocJuego(Juego *game){
    juegoAssoc = game;
}

void Jugador::chocoObstaculoSerp(QPoint gp){
    if(nroTurnos > 0){
        nroTurnos--;
    }else{
        puntos = 0;
        nroTurnos = TURNOS_INICIALES - 1;
        lblValPuntos->setText(QString::number(puntos));
    }
    lblValTurnos->setText(QString::number(nroTurnos));
    juegoAssoc->getMapa()->restaurarMapa();
}

void Jugador::comioAlimento(Alimento tipo){
    Serpiente *ser = juegoAssoc->getMapa()->getSerpiente();
    switch(tipo.alimento){
        case Alimento1: // Aumenta el largo, los puntos y la velocidad
            ser->setLongitud(ser->getLongitud() + incLongitud);
            ser->setVelocidad(ser->getVelocidad() + incTiempo);
            puntos += 1;
            lblValPuntos->setText(QString::number(puntos));
            break;
        case Alimento2://Disminuye tama�o
            if(ser->getLongitud() > ser->getMinLongitud()){
                ser->setLongitud(ser->getLongitud() - incLongitud);
                ser->setVelocidad(ser->getVelocidad() - incTiempo);
            }
            break;
        case Alimento3://Aumenta tama�o y velocidad
            if(ser->getLongitud() < ser->getMaxLongitud()){
                ser->setLongitud(ser->getLongitud() + incLongitud);
            }
            ser->setVelocidad(ser->getVelocidad() + incTiempo);
            break;
        case Alimento4://Disminuye los puntos
            if(puntos > 0){
                puntos -= 1;
                lblValPuntos->setText(QString::number(puntos));
            }
            break;
    }
    juegoAssoc->getMapa()->addAlimAleatorio(1);
}
