#include "juego.h"
#include "ui_juego.h"


Juego::Juego(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Juego)
{
    QPalette paleta(palette());
    paleta.setColor(QPalette::Background, Qt::black);
    /**
      Inicializar el mapa con una dimension de 800x800
      */
    dimension = QPoint(800, 800);
    mapa = new Mapa(dimension, this);
    mapa->setAutoFillBackground(true);
    mapa->setPalette(paleta);

    /**
      Inicializacion del panel de jugador para posicionarlo al lado del mapa
      */
    paleta.setColor(QPalette::Background, Qt::black);
    jugador = new Jugador(this, this);
    jugador->setPalette(paleta);
    jugador->setGeometry(mapa->width(), 0, 200, mapa->height());
    jugador->setAutoFillBackground(true);

    /**
      Configura la ventana de juego
      */
    paleta.setColor(QPalette::Background, Qt::white);
    this->setPalette(paleta);
    this->setFixedSize(mapa->width() + jugador->width(), mapa->height());

    setFocusPolicy(Qt::StrongFocus);

    ui->setupUi(this);
}

Juego::~Juego()
{
    delete ui;
}

void Juego::setJugador(Jugador *player){
    jugador = player;
}

void Juego::setMapa(Mapa *map){
    mapa = map;
}

void Juego::setNivel(int nivel){
    this->nivel = nivel;
}

void Juego::cerrar(){
    this->close();
}

Mapa *Juego::getMapa(){
    return mapa;
}

void Juego::keyPressEvent(QKeyEvent *k){
    int kp = k->key();
    QMessageBox::StandardButton ret;
    Serpiente *serp = mapa->getSerpiente();

    /**
      Actua en base a las teclas presionadas inicialmente
      Puede detener el movimiento o iniciarlo en caso de presionar una tecla de direccion
      */
    switch(kp){
        case Qt::Key_Escape:
            serp->detenerMovimiento();
            ret = QMessageBox::warning(this, "Confirmacion", "�Esta seguro que desea salir del juego?", QMessageBox::Yes | QMessageBox::No);
            if(ret == QMessageBox::Yes){
                this->close();
            }else{
                mapa->getSerpiente()->iniciarMovimiento();
            }
            break;
        case Qt::Key_Up:
            if(serp->getSentido() != Abajo){
                serp->setSentido(Arriba);
            }
            serp->iniciarMovimiento();
            break;
        case Qt::Key_Down:
            if(serp->getSentido() != Arriba){
                serp->setSentido(Abajo);
            }
            serp->iniciarMovimiento();
            break;
        case Qt::Key_Left:
            if(serp->getSentido() != Derecha){
                serp->setSentido(Izquierda);
            }
            serp->iniciarMovimiento();
            break;
        case Qt::Key_Right:
            if(serp->getSentido() != Izquierda){
                serp->setSentido(Derecha);
            }
            serp->iniciarMovimiento();
            break;
    }
}
