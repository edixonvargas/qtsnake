#ifndef JUGADOR_H
#define JUGADOR_H

#include <QLabel>
#include <QWidget>
#include <QKeyEvent>

#include "globales.h"
#include "mapa.h"
#include "juego.h"
#include "serpiente.h"

class Juego;
class Serpiente;
class Jugador : public QWidget
{
        Q_OBJECT

    private:
        const int TURNOS_INICIALES;

        int nroTurnos; //numero de turnos actuales que tiene el jugador
        int puntos; //puntos actuales del jugador
        Juego *juegoAssoc; //juego al cual esta asignado este jugador
        Serpiente *serpControlada; //serpiente que controla este jugador

        /**
          Etiquetas que definen el status del jugador actual
          */
        QLabel *lblPuntos, *lblValPuntos, *lblTurnos, *lblValTurnos;

    public:
        /**
          Inicia un nuevo jugador

          @param juego el juego al cual pertenecera el jugador

          @param parent el objeto donde se posicionara el jugador
          */
        explicit Jugador(Juego *juego, QWidget *parent = 0);
        ~Jugador();

        /**
          Asigna los puntos actuales del jugador

          @param pts puntos que se asociaran actualmente
          */
        void setPuntos(int pts);

        /**
          Obtiene los puntos actuales del jugador
          */
        int getPuntos();

        /**
          Indica a que juego pertenece este jugador, util porque mediante esta
          asociacion se controlaran parametros del juego

          @param game juego actual al que pertenece este jugador
          */
        void assocJuego(Juego *game);

        /**
          Asigna las dimensiones del panel jugador actual

          @param x posicion en el eje X
          @param y posicion en el eje Y
          @param w anchura del panel
          @param h altura del panel
          */
        void setGeometry(int x, int y, int w, int h);

        /**
          Devuelve los turnos actuales del jugador

          @return devuelve los turnos del jugador
          */
        int getTurnos();

    protected slots:

        /**
          Escucha los eventos generados al comer algun alimento

          @param tipo entrega informacion sobre el alimento consumido
          */
        void comioAlimento(Alimento tipo);

        /**
          Escucha eventos de choque de la serpiente

          @param gp entrega la posicion del obstaculo que choco
          */
        void chocoObstaculoSerp(QPoint gp);
};

#endif // JUGADOR_H
