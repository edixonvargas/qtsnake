#include "serpiente.h"

Serpiente::Serpiente(Mapa *map) : maxLongitud(1500), minLongitud(20){
    tVelocidad = new QTimer(this);
    iniciarSerpiente(map);
    connect(tVelocidad, SIGNAL(timeout()), this, SLOT(timeout()));
}

Serpiente::Serpiente(int largo, Mapa *map) : maxLongitud(1500), minLongitud(20){
    QPoint dimMap = map->getDimension();
    tVelocidad = new QTimer(this);
    iniciarSerpiente(map, largo, QPoint(dimMap.x()/2, dimMap.y()/2));
    connect(tVelocidad, SIGNAL(timeout()), this, SLOT(timeout()));
}

void Serpiente::iniciarSerpiente(Mapa *map){
    QPoint dimMap = map->getDimension();

    iniciarSerpiente(map, 5, QPoint(dimMap.x()/2, dimMap.y()/2));
}

void Serpiente::iniciarSerpiente(Mapa *map, int largo, QPoint posicion){
    int compY, compX;

    minIntervalo = 1000;
    longitud = largo;
    posActual = posicion;
    compX = posActual.y();
    compY = posActual.x();
    maxLongitud = 1500;
    setMaxVelocidad(100);
    setMinVelocidad(1);

    setVelocidad(5);

    estructuraSerpiente = QList<QPoint>();
    for(int i = 0; i < largo; i++){
        estructuraSerpiente.append(QPoint(compX, compY));
    }
}

bool Serpiente::hayAlimento(int x, int y){
    return true;
}

void Serpiente::setLongitud(int longit){
    longitud = longit;
}

int Serpiente::getLongitud(){
    return longitud;
}

void Serpiente::setSentido(Direccion sent){
    sentido = sent;
}

void Serpiente::iniciarMovimiento(){
    if(!tVelocidad->isActive()){
        tVelocidad->start();
    }
}

void Serpiente::detenerMovimiento(){
    if(tVelocidad->isActive()){
        tVelocidad->stop();
    }
}

void Serpiente::asociarMapa(Mapa *maps){
    mapAssoc = maps;
}

void Serpiente::verificarAlimento(Alimento alimento, QList<Alimento> *alimentos, QPoint proxPunto, int lSerpiente){
    alimento.posicion = proxPunto;

    if(alimentos->contains(alimento)){
        alimento = alimentos->takeAt(alimentos->indexOf(alimento));
        emit comioAlimento(alimento);
    }
    if(lSerpiente >= longitud && lSerpiente >= minLongitud){
        estructuraSerpiente.removeFirst();
        if(lSerpiente > longitud){
            estructuraSerpiente.removeFirst();
        }
    }
    estructuraSerpiente.append(proxPunto);
    posActual = proxPunto;
}

void Serpiente::mover(Direccion dir){
    QPoint proxPunto;
    Alimento alimento;
    QList<Alimento> *alimentos = mapAssoc->getAlimentos();
    int lSerpiente = estructuraSerpiente.count();

    switch(dir){
        case Neutral:
            tVelocidad->stop();
            break;
        case Arriba:
            proxPunto = QPoint(posActual.x(), posActual.y() - 1);
            if(mapAssoc->getDiagramaActual().contains(proxPunto) || estructuraSerpiente.contains(proxPunto)){
                tVelocidad->stop();
                emit chocoObstaculo(proxPunto);
            }else{
                verificarAlimento(alimento, alimentos, proxPunto, lSerpiente);
            }
            break;
        case Abajo:
            proxPunto = QPoint(posActual.x(), posActual.y() + 1);
            if(mapAssoc->getDiagramaActual().contains(proxPunto) || estructuraSerpiente.contains(proxPunto)){
                tVelocidad->stop();
                emit chocoObstaculo(proxPunto);
            }else{
                verificarAlimento(alimento, alimentos, proxPunto, lSerpiente);
            }
            break;
        case Derecha:
            proxPunto = QPoint(posActual.x() + 1, posActual.y());
            if(mapAssoc->getDiagramaActual().contains(proxPunto) || estructuraSerpiente.contains(proxPunto)){
                tVelocidad->stop();
                emit chocoObstaculo(proxPunto);
            }else{
                verificarAlimento(alimento, alimentos, proxPunto, lSerpiente);
            }
            break;
        case Izquierda:
            proxPunto = QPoint(posActual.x() - 1, posActual.y());
            if(mapAssoc->getDiagramaActual().contains(proxPunto) || estructuraSerpiente.contains(proxPunto)){
                tVelocidad->stop();
                emit chocoObstaculo(proxPunto);
            }else{
                verificarAlimento(alimento, alimentos, proxPunto, lSerpiente);
            }
            break;
    }
    mapAssoc->repaint();
}

void Serpiente::alargaSerpiente(int cant){
    if(longitud + cant < maxLongitud){
        longitud += cant;
    }
}

int Serpiente::longActual(){
    return (int)(sizeof(estructuraSerpiente)/sizeof(QPoint));
}

int Serpiente::distDosPuntos(QPoint p1, QPoint p2){
    return (int)(sqrt(((p1.x() - p2.x())^2) + ((p1.y() - p2.y())^2)));
}

QList<QPoint> Serpiente::getEstructura(){
    return estructuraSerpiente;
}

void Serpiente::timeout(){
    mover(sentido);
}

Direccion Serpiente::getSentido(){
    return sentido;
}

void Serpiente::setMinVelocidad(double minvel){
    maxIntervalo = (1/ minvel) * 1000;//establece el intervalo maximo del timer tVelocidad esto quiere decir que se recorrera 1 paso por segundo
}

void Serpiente::setMaxVelocidad(double maxvel){
    minIntervalo = (1/ maxvel) * 1000;
}

void Serpiente::setVelocidad(double vel){
    double i = (1 / vel) * 1000;
//    int iActual = tVelocidad->interval();
    if(i >= minIntervalo && i <= maxIntervalo){
        tVelocidad->setInterval(i);
    }
}

double Serpiente::getVelocidad(){
    double vel = (1 / (double)tVelocidad->interval()) * 1000;
    return vel;
}

int Serpiente::getMinLongitud(){
    return minLongitud;
}

void Serpiente::setMinLongitud(int min){
    minLongitud = min;
}

int Serpiente::getMaxLongitud(){
    return maxLongitud;
}

void Serpiente::restaurarSerpiente(){
    QPoint dimMap = mapAssoc->getDimension();
    iniciarSerpiente(mapAssoc, 20, QPoint(dimMap.x()/2, dimMap.y()/2));
}

bool Serpiente::isMoving(){
    return tVelocidad->isActive();
}
