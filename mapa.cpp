#include "mapa.h"

Mapa::Mapa(QWidget *parent) : QWidget(parent), anchoMapa(AN_MAPA), altoMapa(AL_MAPA){
	serp = new Serpiente(this);
	iniciarMapa(QPoint(800, 800), 10);
}

Mapa::Mapa(QPoint dim, QWidget *parent): QWidget(parent), anchoMapa(AN_MAPA), altoMapa(AL_MAPA){
	serp = new Serpiente(this);
	tAlimentos.start(5000);
	iniciarMapa(dim, numAlimentos);
}

void Mapa::iniciarMapa(QPoint dim, int nalimalea){
	setGeometry(0, 0, dim.x(), dim.y());

	//Establece las dimensiones de cada celda
	dimBloque.setX(this->width()/anchoMapa);
	dimBloque.setY(this->height()/altoMapa);

	//Asigna propiedades a la serpiente en este mapa, tales como longitud y el mapa al que pertenece
	serp->asociarMapa(this);
	serp->setLongitud(20);
	serp->restaurarSerpiente();

	cargarMapa();
	posicionarAlimAleatorio(nalimalea);
	connect(&tAlimentos, SIGNAL(timeout()), this, SLOT(timeout()));
}

void Mapa::posicionarAlimento(Alimento alim){
	alimentos.append(alim);
}

void Mapa::posicionarAlimAleatorio(int nroAlimentos){
	Alimento al;
	QList<QPoint> *estSerp= &serp->getEstructura();
	qsrand(QTime::currentTime().msec());
	alimentos.clear();
	for(int i = 0; i < nroAlimentos; i++){
		do{ //Se genera un numero aleatorio y se verifica si este existe en la lista de obstaculos o alimentos.
			al.alimento = (TipoAlimento)int(round(4 * qrand() / 32768));
			al.posicion = QPoint(anchoMapa * qrand() / 32768, altoMapa * qrand() / 32768);
		}while(estSerp->contains(al.posicion) || obs.contains(al.posicion) || alimentos.contains(al));
		posicionarAlimento(al);
	}
}

void Mapa::cargarMapa(){
	int per = (2*anchoMapa - 1) + (2*altoMapa - 3);

	for(int i = 0; i < per; i++){
		if(i >= 0 && i < anchoMapa){
			obs.append(QPoint(i, 0));
		}else if(i >= anchoMapa && i < (anchoMapa + altoMapa - 1)){
			obs.append(QPoint(anchoMapa-1, i-altoMapa+1));
		}else if(i >= (anchoMapa + altoMapa - 1) && i < (2*anchoMapa + altoMapa -2)){
			obs.append(QPoint(i - (anchoMapa + altoMapa - 1), altoMapa - 1));
		}else{
			obs.append(QPoint(0, i-(2*anchoMapa + altoMapa - 3)));
		}
	}
}

void Mapa::paintEvent(QPaintEvent *event){
	/**
Redibuja el mapa completamente junto con la serpiente
Se trata de tomar todas las listas de elementos (obstaculos, alimentos y serpiente) y
recorrerlas una a una dibujando cada celda en su correspondiente posicion en el mapa
	*/
	QPainter pincel;
	QPen circlePen;

	circlePen.setWidth(0);
	pincel.begin(this);
	pincel.setRenderHint(QPainter::Antialiasing);

	circlePen.setColor(Qt::gray);
	pincel.setBrush(Qt::gray);
	pincel.setPen(circlePen);
	QPoint b;
	for(int i = 0; i < obs.count(); i++){
		b = obs.at(i);
		pincel.drawRect(QRect(b.x()*dimBloque.x(), b.y()*dimBloque.y(), dimBloque.x(), dimBloque.y()));
	}

	pincel.setBrush(Qt::yellow);
	circlePen.setColor(Qt::yellow);
	pincel.setPen(circlePen);

	QList<QPoint> estSerp = serp->getEstructura();
	QPoint pAct;
	for(int i = 0; i < estSerp.count(); i++){
		pAct = estSerp.at(i);
		pincel.drawRect(pAct.x()*dimBloque.x(), pAct.y()*dimBloque.y(), dimBloque.x(), dimBloque.y());
	}

	Alimento al;
	for(int i = 0; i < alimentos.count(); i++){
		al = alimentos.at(i);
		switch(al.alimento){
			case Alimento1:
				pincel.setBrush(Qt::green);
				circlePen.setColor(Qt::green);
				break;
			case Alimento2:
				pincel.setBrush(Qt::blue);
				circlePen.setColor(Qt::blue);
				break;
			case Alimento3:
				pincel.setBrush(Qt::red);
				circlePen.setColor(Qt::red);
				break;
			case Alimento4:
				pincel.setBrush(Qt::darkRed);
				circlePen.setColor(Qt::darkRed);
				break;
			default:
				pincel.setBrush(Qt::darkRed);
				circlePen.setColor(Qt::darkRed);
				break;
		}
		pincel.setPen(circlePen);
		pincel.drawRect(al.posicion.x()*dimBloque.x(), al.posicion.y()*dimBloque.y(), dimBloque.x(), dimBloque.y());
	}
	pincel.end();
}

Obstaculos Mapa::getDiagramaActual(){
	return obs;
}

QPoint Mapa::getDimension(){
	return QPoint(anchoMapa, altoMapa);
}

QPoint Mapa::getDimBloque(){
	return dimBloque;
}

QList<Alimento> *Mapa::getAlimentos(){
	return &alimentos;
}

Serpiente *Mapa::getSerpiente(){
	return serp;
}

void Mapa::timeout(){
	//Simplemente elimina el primer alimento de la lista
	//y a�ade uno al final
	if(serp->isMoving()){
		alimentos.removeFirst();
		addAlimAleatorio(1);
	}
}

void Mapa::restaurarMapa(){
	const int lserp = 20;
	iniciarMapa(QPoint(this->geometry().width(), this->geometry().height()), numAlimentos);
	serp->setLongitud(lserp);
}

void Mapa::addAlimAleatorio(int cant){
	int r;
	double pr;
	Alimento al;
	QList<QPoint> *estSerp = &serp->getEstructura();

	for(int i = 0; i < cant; i++){
		do{//Al igual que posicionarAlimAleatorio pero en este caso simplemente agrega aleatoriamente a la lista
			r = qrand();
			pr = 4 * (double)r / 32768;
			al.alimento = (TipoAlimento)int(pr);
			al.posicion = QPoint(anchoMapa * qrand() / 32768, altoMapa * qrand() / 32768);
		}while(estSerp->contains(al.posicion) || obs.contains(al.posicion));
		posicionarAlimento(al);
	}
}

