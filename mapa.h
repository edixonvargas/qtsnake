#ifndef MAPA_H
#define MAPA_H

#include <QTimer>
#include <QTime>
#include <QPainter>
#include <math.h>
//#include <globales.h>
#include "juego.h"

/**
  Define una lista de obstaculos
  */
typedef QList<QPoint> Obstaculos;

class Serpiente;
class Mapa : public QWidget
{
    Q_OBJECT
    public:
        //Tipos Abstractos de Datos

        //Declaracion de los constructores

        /**
          Crea una nueva instancia de Mapa aceptando solo como parametro el objeto que lo contendra
          @param parent objeto contenedor de este Mapa
          */
        explicit Mapa(QWidget *parent = 0);

        /**
          Crea una instancia de Mapa con las dimensiones preestablecidas

          @param dim dimensiones del Mapa
          @param parent objeto contenedor de este Mapa
          */
        explicit Mapa(QPoint dim, QWidget *parent = 0);

        //Declaracion de los metodos publicos

        /**
          Posiciona aleatoriamente un numero indicado de alimentos

          @param nroAlimentos especifica el numero de alimentos que seran posicionados de una vez
          */
        void posicionarAlimAleatorio(int nroAlimentos);

        /**
          Obtiene la dimencion (celdas) del mapa actual

          @return objeto QPoint que contiene la dimension en ancho y alto de este mapa
          */
        QPoint getDimension();

        /**
          Obtiene la dimension actual de los bloques (celdas) del mapa

          @return objeto QPoint que devuelve el ancho y alto (X y Y respectivamente)
          */
        QPoint getDimBloque();

        /**
          Obtiene el diagrama actual (bloques que funcionan como obstaculos) del mapa

          @return devuelve una lista de obstaculos
          */
        Obstaculos getDiagramaActual();

        /**
          Obtiene una lista de alimentos posicionados actualmente en el mapa

          @return devuelve una lista de tipo Alimento que contiene todos los alimentos
    visibles en el mapa con sus posiciones individuales
          */
        QList<Alimento> *getAlimentos();

        /**
          Obtiene la serpiente que esta actualmente participando en el mapa

          @return devuelve un objeto del tipo Serpiente que hace referencia a la serpiente en juego actualmente
          */
        Serpiente *getSerpiente();

        /**
          Devuelve el mapa a su estado original y posiciona los alimentos nuevamente
          */
        void restaurarMapa();

        /**
          Agrega alimentos aleatoriamente a la lista de alimentos actuales

          @param cant indica el numero de alimentos que se agregaran
          */
        void addAlimAleatorio(int cant);

    private:

        //Declaracion de las constantes privadas
        const int anchoMapa;    //Contiene el numero de celdas horizontales que tendra el mapa
        const int altoMapa;     //Contiene el numero de celdas verticales que tendra el mapa

        //Declaracion de las variables privadas
        Serpiente *serp;        //Contiene la referencia a la serpiente que esta en juego
        QPoint dimBloque;       //Contiene la dimension de los bloques

        Obstaculos obs;         //Contiene una lista de obstaculos presentes en el mapa
        QList<Alimento> alimentos;  //Contiene una lista de alimentos presentes actualmente en el mapa
        QTimer tAlimentos;      //Representa el timer que cambia la posicion constantemente de los alimentos

        //Declaracion de los metodos privados

        /**
          Posiciona un alimento con las especificaciones indicadas en el parametro

          @param alim contiene las propiedades del alimento a posicionar,
    este se a�adira a la lista de alimentos actuales
          */
        void posicionarAlimento(Alimento alim);

        /**
          Posiciona todos los obstaculos del mapa actual
          */
        void cargarMapa();

        /**
          Inicializa el mapa actual con los parametros especificados por el usuario

          @param dim establece la dimension real del mapa

          @param nalimalea indica el numero de alimentos que tendra el mapa inicialmente
          */
        void iniciarMapa(QPoint dim, int nalimalea);

    protected:

        /**
          Este evento es llamado cada ves que se dibuja el mapa (llamado por el sistema operativo)

          @param event devuelve los detalles del evento actual
          */
        void paintEvent(QPaintEvent *event);


    protected slots:

        /**
          Escucha las se�ales generadas por el temporizador de alimentos
          */
        void timeout();

};

#endif

