#ifndef JUEGO_H
#define JUEGO_H

#include <QMainWindow>
#include <QMessageBox>

#include "globales.h"
#include "mapa.h"
#include "serpiente.h"
#include "jugador.h"

#define incLongitud 10 //Cuantas unidades crece/reduce la serpiente cuando come un alimento 1, 2 o 3
#define incTiempo 1
#define numAlimentos 10 //Numero de alimentos iniciales en el mapa

namespace Ui {
    class Juego;
}

class Jugador;
class Mapa;

/**
@name Juego Es la clase principal de la interfaz grafica, controla todas las caracteristicas del juego en general
asi como la inicializacion de los actores en el mismo.
  */
class Juego : public QMainWindow
{
    Q_OBJECT
    public:
        /**
          @name Juego Instancia un nuevo juego dentro de un objeto QWidget
          */
        explicit Juego(QWidget *parent = 0);
        ~Juego();

        /**
          Asigna el jugador actual

          @param player el jugador que sera asignado del tipo Jugador
          @see clase Jugador
          */
        void setJugador(Jugador *player);

        /**
          Asigna un mapa al juego actual

          @param map el mapa que sera asignado actualmente, debe ser del tipo Mapa
          @see clase Mapa
          */
        void setMapa(Mapa *map);

        /**
          Asigna el nivel inicial del juego
          @param nivel indica el nivel actual, este valor debe ser menor o igual que la cantidad de niveles disponibles
          */
        void setNivel(int nivel);

        /**
          Cierra el juego actual y libera la memoria
          */
        void cerrar();

        /**
          Devuelve el mapa en el que se esta jugando

          @return Mapa
          */
        Mapa *getMapa();

    private:

		Ui::Juego *ui;

        QPoint dimension; //dimension del mapa
        Mapa *mapa; //Mapa que se asignara a este juego
        Jugador *jugador; //Jugador que se asignara a este juego
        int nivel; //nivel actual


    protected:

        /**
          Captura los eventos del teclado permitiendo controlar el inicio y demas funciones del juego

          @param k entrega datos sobre la tecla presionada
          */
        void keyPressEvent(QKeyEvent *k);

};

#endif // JUEGO_H
