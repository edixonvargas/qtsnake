/**
  @note Este programa se suministra con fines educativos y no comerciales,
  no ha sido completamente verificado, por lo tanto puede contener fallas
  VARPER Tech no se hace responsable por el uso que se le de a este software,
  si este llegara a causar da�os a su equipo es responsabilidad exclusivamente
  suya.
  @author Ing. Edixon Vargas. Presidente VARPER Tech
  e-mail: edixon.eduardo.vargas@hotmail.com
  sitio web: http://varper-tech.com/
  @date 29/11/2011
  */
#ifndef GLOBALES_H
#define GLOBALES_H

#include <QPoint>
#include <QList>

#define AN_MAPA 80
#define AL_MAPA 80


/**
  Establece las posibles constantes para las direcciones de la serpiente
  */
enum Direccion{
    Neutral = 0,
    Arriba,
    Abajo,
    Derecha,
    Izquierda
};

/**
  Indica el tipo de alimentos posibles en el mapa

  @param Alimento1 este es un alimento bueno, si se consume la serpiente crecera,
  aumentara su velocidad y aumentara su puntaje

  @param Alimento2 es un alimento bueno que reduce el tama�o y la velocidad

  @param Alimento3 es un alimento malo que solo aumenta la velocidad y el tama�o,
  incrementando asi la dificultad

  @param Alimento4 este es negativo y unicamente reduce los puntos acumulados
  */
enum TipoAlimento{
    Alimento1,
    Alimento2,
    Alimento3,
    Alimento4
};

/**
  Describe las caracteristicas de un alimento, las cuales son la posicion y el tipo
  */
typedef struct Al{
        TipoAlimento alimento;
        QPoint posicion;

        bool operator==(Al pos){
            return posicion == pos.posicion;
        }
}Alimento;

#endif // GLOBALES_H
